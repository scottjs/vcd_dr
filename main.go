package main

import (
	"flag"
	"fmt"
	"log"
	"os"
)

const apiversion = "5.6"

var (
	version string
	buildDate string
)

func main() {
	var u, p, o, url, d, n string
	var t, f, c, s, v, ok bool
	var l int
	var conf *Config
	var cl *CloudConfig
	var err error

	flag.StringVar(&u, "u", "", "Username for vCA")
	flag.StringVar(&p, "p", "", "Password for vCA")
	flag.StringVar(&o, "o", "", "vCD Org Name")
	flag.StringVar(&url, "url", "", "vCD URL, e.g: https://pXvY-vcd.vchs.vmware.com/")
	flag.StringVar(&d, "d", "", "Filename to export the server-side replication UUIDS/VMnames")
	flag.StringVar(&n, "n", "", "Name of the cloud from the config file to perform actions against")
	flag.BoolVar(&t, "t", false, "Conduct a test failover")
	flag.BoolVar(&f, "f", false, "Conduct a production failover")
	flag.BoolVar(&s, "server", false, "Run app in HTTP server mode")
	flag.BoolVar(&c, "c", false, `Load config.json file 
	Note: Config file takes precedence over other vCD property flags`)
	flag.BoolVar(&v, "v", false, "Displays application version")
	flag.IntVar(&l, "l", 2, "Logging level\n\t1 - Debug\n\t2 - Info\n\t3 - Error")
	flag.Parse()

	logfile := InitLogging(l)
	defer logfile.Close()

	if v {
		printVersion()
		return
	}

	if t && f {
		fmt.Println("Cannot execute Test and Fail simultaneously")
		return
	}

	if s {
		conf, err := LoadConfig()
		if err != nil {
			Error(err.Error())
			fmt.Println("Error loading conf, see log file")
			return
		}
		StartServer(conf)
		return
	}

	if c {
		conf, err = LoadConfig()
		if err != nil {
			Error(err.Error())
			return
		}
		if conf.Clouds == nil {
			fmt.Println("No clouds defined in config")
			return
		}
		cl, ok = conf.Clouds[n]
		if !ok {
			fmt.Printf("No cloud with name %s\n", n)
			return
		}
		u = cl.Username
		p = cl.Password
		o = cl.OrgName
		url = cl.URL
	}

	if d != "" {
		DumpDRDetails(d, u, p, o, url)
		return
	}

	if conf == nil && (t || f) {
		fmt.Println("Cannot test or failover without config file")
		return
	} else if t {
		DoFailover(cl, true)
		return
	} else if f {
		DoFailover(cl, false)
		return
	}
	fmt.Println("Use the -h flag for full help on command arguments")
}

//InitLogging sets up the log output file and logging level
func InitLogging(l int) *os.File {
	SetLogLevel(l)
	f, err := os.Create("cerberus.log")
	if err != nil {
		fmt.Println("Cannot create log file")
		panic(err)
	}
	log.SetOutput(f)
	return f
}

func printVersion() {
	fmt.Println("Date: ", buildDate)
	fmt.Println("Version: ", version)
}

//DumpDRDetails will log into vCD and gather the VM UUID/Name details of
//the environment and output them to a file
func DumpDRDetails(filename, user, pass, orgname, url string) {
	s := NewSession(user, pass, orgname, url, apiversion)
	f, err := os.Create(filename)
	Info("Exporting vCA DR details to file...")
	if err != nil {
		Error("Could not create output file for details")
		Error(err.Error())
		return
	}
	defer f.Close()
	org, err := s.PopulateCloudDetails()
	if err != nil {
		Error(err.Error())
		return
	}

	for _, rep := range org.Replications {
		f.WriteString(fmt.Sprintf("%v -- %v\n", rep.UUID, rep.Name))
	}
	Info(fmt.Sprintf("Data saved to file %v", filename))
}

//DoFailover executes a production DR failover event using the
//provided config data
func DoFailover(c *CloudConfig, test bool) {
	s := NewSession(c.Username, c.Password, c.OrgName, c.URL, apiversion)
	Info("Beginning Test/Failover process")
	org, err := s.PopulateCloudDetails()
	if err != nil {
		Error(err.Error())
		return
	}

	for _, cg := range c.Groups { //Loop over the configured replication groups
		err := org.DisasterFailover(cg, test, true, false, 1)
		if err != nil {
			break
		}
	}
	Info("Test/Failover Operations Complete")
}
