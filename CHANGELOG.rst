Cerberus Changelog
==================

Version 0.4 (2017-02-14)
------------------------

**New Features**

- Version command-line flag (-v)

**Improvements**

- The command-line failover operation now uses the same mechanism as the web version
- Additional error logging for HTTP server crash / start failures
- The orphan VM list now remains sorted at all times
- Added additional save button on the groups control bar

**Bugfixes**

- Added concurrent protection to the map of replications to avoid panic due to
  concurrent access during discovery operations

Version 0.3 (2017-02-04)
------------------------

**New Features**

- DR operations are now queued and run in sequence
- Multiple concurrent VM failover is available
- Added ability to toggle Power On / Ignore Errors functionality when failing over
- Log display is now available in the UI for most recent log events
- Can now reorder replication groups in the UI

**Bugfixes**

- Forced new cloud name to be only alphanumerics
- Fixed possible deadlock condition in server
- Fixed the auto-retry feature when receiving a 403 error
- Fixed loading replication data so it would adhere to the maximum concurrent setting
- XMLHTTP requests will properly show 401 if session has timed out instead of hanging

Version 0.2 (2017-01-17)
------------------------

**Known Issues**

- Possible race conditions during server cache operations that aren't
  protected by a mutex.

**New Features**

- HTTP Server / Web Interface / API Endpoints
- Auth into the web UI is required

**Improvements**

- Fetching cloud data is much faster, now using goroutines
- Mutexes in use during replication fetching to avoid race conditions

Version 0.1 (2017-01-05)
------------------------

- Initial Release
