function NewCloudShell() {
    var nc = {
        Username: "",
        Password: "",
        URL: "",
        OrgName: "",
        Groups: [],
        Orphans: [],
        Mappings: {},
        show: true,
        loading: false,
        showSettings: true,
        powerOn: false,
        ignoreErrors: false
    };
    return nc;
}

function GetClouds(vue) {
    vue.$http.get('/clouds/').then((response) => {
        var clouds, jData, cname;
        jData = response.body;
        clouds = {};
        for (cname in jData) {
            clouds[cname] = Object.assign(NewCloudShell(), jData[cname].Cloud);
            clouds[cname].Orphans = sortOrphans(jData[cname].Orphans,jData[cname].Mappings) || [];
            clouds[cname].Mappings = jData[cname].Mappings || {};
        }
        vue.clouds = clouds;
    }, (response) => {
        vue.clouds = {};
        vue.mType = 'Error:';
        vue.message = 'Unable to retrieve cloud data. Reason: ' + response.body;
    });
}

function GetCloud(elem, vue) {
    var name = elem.dataset.cloud;
    vue.clouds[name].loading = true;
    vue.$http.get('/clouds/' + name, { timeout: 0 }).then((response) => {
        //success
        var jData = response.body;
        if (jData.Cloud.Groups == null) {
            jData.Cloud.Groups = [];
        }
        Object.assign(vue.clouds[name], jData.Cloud)
        jData.Orphans = sortOrphans(jData.Orphans, jData.Mappings)
        vue.clouds[name].Orphans = jData.Orphans || [];
        vue.clouds[name].Mappings = jData.Mappings || {};
        vue.clouds[name].loading = false;

        vue.mType = 'Info:';
        vue.message = 'Cloud refresh successful!';
        vcaTries = 0;
    }, (response) => {
        //Failure
        var msg = response.body;
        if (response.status == 503) {
            if (vcaTries < 10) {
                vcaTries++;
                vue.mType = 'Info:'
                vue.message = 'Refresh data not available. Retrying in 10 seconds.'
                sleep(10000).then(() => { GetCloud(elem, vue); });
                return;
            }
            msg = 'Maximum retries (10) exceeded before data was available.';
        }
        vue.clouds[name].loading = false;
        vue.mType = 'Error:';
        vue.message = 'Cloud refresh error. Reason: ' + msg;
        vcaTries = 0;
    });
}

function AddCloud(vue) {
    var ncName = document.getElementById("addCloud").value;
    ncName = ncName.replace(/[^a-zA-Z0-9]/g, "")
    if (ncName == "") {
        v.mType = 'Error';
        v.message = 'Please provide a name. Use only alphanumeric characters.';
        return;
    }
    var newCloud = {
        name: ncName
    };
    vue.$http.post('/clouds/', newCloud).then((response) => {
        //Success
        vue.$set(vue.clouds, ncName, NewCloudShell());
        h.addCloud = false;
    }, (response) => {
        //Failure
        v.mType = 'Error:';
        v.message = 'Failed to create cloud. Reason: ' + response.body;
    });
}

function UpdateCloud(elem, vue) {
    var name = elem.dataset.cloud;
    var c = vue.clouds[name];
    vue.$http.put('/clouds/' + name, c).then((response) => {
        //Success
        vue.mType = 'Info:';
        vue.message = 'Saved successfully';
    }, (response) => {
        //Failure
        vue.mType = 'Error:';
        vue.message = 'Unable to save changes. Reason:' + response.body;
    });
    return true;
}

function AddRepGroup(elem, vue) {
    var name = elem.dataset.cloud;
    if (vue.clouds[name].Groups == null) {
        vue.clouds[name].Groups = [];
    }
    vue.clouds[name].Groups.push([]);
}

function DelRepGroup(elem, vue) {
    var idx = elem.dataset.idx;
    var name = elem.dataset.name;

    var o = vue.clouds[name].Groups.splice(idx, 1)[0];
    vue.clouds[name].Orphans = sortOrphans(vue.clouds[name].Orphans.concat(o),
        vue.clouds[name].Mappings);
}

function AddItems(elem, vue) {
    var name = elem.dataset.name;
    var idx = elem.dataset.idx;
    var orphs = document.getElementsByName(name + 'Orphans')[0];
    var selected = [];
    for (var i = 0; i < orphs.selectedOptions.length; i++) {
        selected.push(orphs.selectedOptions[i].value);
    }

    //Deselect all the values after moving them
    for (var x = 0; x < orphs.options.length; x++) {
        orphs.options[x].selected = false;
    }

    orphs = vue.clouds[name].Orphans;
    var grp = vue.clouds[name].Groups[idx];
    for (var x in selected) {
        var uuid = selected[x];
        var position = orphs.indexOf(uuid);
        if (position > -1) {
            orphs.splice(position, 1);
        }
        grp.push(uuid);
    }
}

function RemItems(elem, vue) {
    var name = elem.dataset.name;
    var idx = elem.dataset.idx;
    var grp = vue.clouds[name].Groups[idx];
    var orphs = vue.clouds[name].Orphans;
    var mapp = vue.clouds[name].Mappings
    var grpBox = document.getElementsByName(name + 'RG' + idx)[0];
    var selected = [];
    for (var i = 0; i < grpBox.selectedOptions.length; i++) {
        selected.push(grpBox.selectedOptions[i].value);
    }

    //Deselect all the values after moving them
    for (var x = 0; x < grpBox.options.length; x++) {
        grpBox.options[x].selected = false;
    }

    for (var x in selected) {
        var uuid = selected[x];
        var position = grp.indexOf(uuid);
        if (position > -1) {
            grp.splice(position, 1);
        }
        orphs.push(uuid);
        orphs = sortOrphans(orphs, mapp);
    }
}

function Failover(elem, vue, test, total) {
    //Test is a boolean for whether this is a test fail or actual
    //Total is a boolean for if it's just a group or all groups
    var name = elem.dataset.name;
    var idx = elem.dataset.idx;
    var payload = {
        servers: [],
        powerOn: false,
        ignoreErrors: false,
        maxConcurrent: 1
    };
    var endpoint;

    if (!confirm("Are you sure you want to initiate a test/failover action?")) {
        return;
    }

    if (!total) {
        payload.servers = vue.clouds[name].Groups[idx];
    } else {
        for (var i = 0; i < vue.clouds[name].Groups.length; i++) {
            payload.servers = payload.servers.concat(vue.clouds[name].Groups[i]);
        }
    }
    payload.powerOn = vue.clouds[name].powerOn;
    payload.ignoreErrors = vue.clouds[name].ignoreErrors;
    payload.maxConcurrent = parseInt(document.getElementById(name + "maxC").value)

    if (payload.servers.length == 0) {
        vue.mType = 'Error:';
        vue.message = 'No servers selected for failover.';
        return
    }

    if (test) {
        endpoint = '/clouds/' + name + '/testFailover/';
    } else {
        endpoint = '/clouds/' + name + '/Failover/';
    }

    vue.$http.post(endpoint, payload).then((response) => {
        //Success
        vue.mType = 'Info:';
        vue.message = 'Action has been initiated. Check vCloud Air for status.';
    }, (response) => {
        //Failure
        vue.mType = 'Error:';
        vue.message = 'Failed to initiate failover action. Reason: ' + response.body;
    });
}

function GetLog() {
    v.$http.get('/log').then((response) => {
        //Success
        v.mType = 'Latest Log Entries:';
        var msg = response.body.replace(/\n/g, "<br>");
        v.message = msg;
    }, (response) => {
        //Failure
        v.mType = 'Error:';
        v.message = 'Unable to get log. Reason: ' + response.body;
    });
}

function swapArrayValues(arr, nIdx, oIdx) {
    if (nIdx < 0 || nIdx >= arr.length) {
        return;
    }
    var temp = arr[nIdx];
    arr.splice(nIdx, 1);
    arr.splice(oIdx, 0, temp);
}

function sortOrphans(orphs, map) {
    if (orphs == null) {
        return orphs;
    }
    return orphs.sort((a, b) => {
        var aa = map[a].toLowerCase();
        var bb = map[b].toLowerCase();

        if (aa == bb) {
            return 0;
        } else if (aa > bb) {
            return 1;
        } else {
            return -1;
        }
    });
}

function sleep(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}
