package main

import (
	"bytes"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"sync"
	"time"

	"github.com/pkg/errors"
)

const uuidl = 36
const vrnamespace = "http://www.vmware.com/vr/v6.0"

type missingAction struct {
}

func (ma missingAction) Error() string {
	return "Could not locate requested action"
}

//Session is the struct to represent a vCD user session
type Session struct {
	Username string
	Password string
	token    string
	OrgName  string
	URL      string
	client   http.Client
	headers  http.Header
}

//NewSession creates a new vCD session object based on provided
//parameters
func NewSession(username, password, orgName, vcdurl, version string) *Session {
	accept := fmt.Sprintf("application/*+xml;version=%s", version)
	session := &Session{
		Username: username,
		Password: password,
		OrgName:  orgName,
		URL:      vcdurl,
		client:   http.Client{},
		headers:  make(http.Header),
	}
	session.headers.Set("Accept", accept)
	session.headers.Set("x-vcloud-authorization", "")
	return session
}

//Login to a vCD instance and generate an authorization token
func (s *Session) Login() ([]Link, error) {
	loginURL := s.URL + "/api/sessions"
	username := s.Username + "@" + s.OrgName
	req, err := http.NewRequest("POST", loginURL, nil)
	Debug("Generating request object for login")
	if err != nil {
		return nil, errors.Wrap(err, "Could not generate HTTP request object")
	}
	req.SetBasicAuth(username, s.Password)
	s.token = ""
	Debug("Sending login API request")
	resp, err := s.DoAPIRequest(req)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to send login API request")
	}
	s.token = resp.Header.Get("x-vcloud-authorization")
	s.headers.Set("x-vcloud-authorization", s.token)
	var links struct {
		Links []Link `xml:"Link"`
	}

	body, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		return nil, errors.Wrap(err, "Failed to read login response body")
	}

	Debug("Unmarshaling login xml body")
	err = xml.Unmarshal(body, &links)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to unmarshal login xml body")
	}
	return links.Links, nil
}

//FetchAndUnmarshal fetches vCD API data from the provided URL
//and unmarshals the XML into the provided struct reference
func (s *Session) FetchAndUnmarshal(url string, v interface{}) error {
	resp, err := s.Get(url)
	if err != nil {
		return errors.Wrapf(err, "Failed API Get request. URL: %v", url)
	}
	return Unmarshal(resp, v)
}

//Unmarshal attempts to unmarshal an XML HTTP response into a provided struct
func Unmarshal(resp *http.Response, v interface{}) error {
	Debug("Unmarshalling HTTP XML response")
	body, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		return errors.Wrap(err, "Failed to read response body")
	}
	err = xml.Unmarshal(body, v)
	if err != nil {
		return errors.Wrap(err, "Failed to unmarshal XML body")
	}
	return nil
}

//Get issues an HTTP Get request through the vCD session
func (s *Session) Get(url string) (*http.Response, error) {
	req, _ := http.NewRequest("GET", url, nil)
	return s.DoAPIRequest(req)
}

//GetOrg returns an a vCD Org object
func (s *Session) GetOrg(orgURL string) (*Org, error) {
	org := new(Org)
	Debug("Gathering Org details")
	err := s.FetchAndUnmarshal(orgURL, org)
	if err != nil {
		return nil, errors.Wrap(err, "Failed gathering Org details")
	}
	org.session = s
	return org, nil

}

//Post issues an HTTP Post request through the vCD session
func (s *Session) Post(url string, body []byte) (*http.Response, error) {
	rdr := bytes.NewReader(body)
	req, _ := http.NewRequest("POST", url, rdr)
	return s.DoAPIRequest(req)
}

//DoAPIRequest is an internal method that issues HTTP requests and adds appropriate
//vCD headers into the request before sending
func (s *Session) DoAPIRequest(req *http.Request) (*http.Response, error) {
	var haveRetried bool
	Debug("Sending HTTP API request")
	for {
		for k, v := range s.headers {
			req.Header[k] = v
		}
		resp, err := s.client.Do(req)
		if err != nil || resp.StatusCode > 399 {
			if err == nil {
				if resp.StatusCode == 403 && !haveRetried {
					Info("Received 403 -- Generating new token and trying again")
					haveRetried = true
					s.Login()
					continue
				}
				errBody, _ := ioutil.ReadAll(resp.Body)
				err = errors.Errorf("HTTP Error: %v, %v\n%s", resp.StatusCode, resp.Status, errBody)
				resp.Body.Close()
			}
			return nil, errors.Wrap(err, "Failed to perform API request")
		}
		return resp, nil
	}
}

//PopulateCloudDetails attempts to log into the vCD instance and populate the
//org details and replications. It returns a fully populated Org pointer
func (s *Session) PopulateCloudDetails() (*Org, error) {
	Info("Populating Cloud Details")
	links, err := s.Login()
	if err != nil {
		return nil, errors.Wrap(err, "Error logging into cloud")
	}

	var orgurl string
	for _, link := range links {
		if strings.ToLower(link.Name) == strings.ToLower(s.OrgName) {
			orgurl = link.URL
			break
		}
	}

	org, err := s.GetOrg(orgurl)
	if err != nil {
		return nil, errors.Wrap(err, "Error populating cloud org")
	}

	err = org.DiscoverReplications()
	if err != nil {
		Error("Error discovering replications")
		return nil, errors.Wrap(err, "Failed to populate replications")
	}
	return org, nil
}

//CommonProps is a struct containing the common XML attributes for most
//VCD types as well as a session object pointer for the current session
type CommonProps struct {
	Name    string `xml:"name,attr"`
	URL     string `xml:"href,attr"`
	ObjType string `xml:"type,attr"`
	session *Session
}

//Link represents a vCD XML <Link> object
type Link struct {
	CommonProps
	Action string `xml:"rel,attr"`
}

//Task represents a vCD asynchronous task object
type Task struct {
	VMName string
	CommonProps
	Progress int
	Error    struct {
		Message string `xml:"message,attr"`
	} `xml:"Error"`
}

//Refresh will query the API and refresh the task object, usually
//for checking the task progress
func (t *Task) Refresh() {
	if err := t.session.FetchAndUnmarshal(t.URL, t); err != nil {
		Error("Failed to fetch and refresh task")
	}
}

//Org represents a vCD <Org> object
type Org struct {
	XMLName xml.Name `xml:"Org"`
	Links   []Link   `xml:"Link"`
	CommonProps
	Replications map[string]*ReplicationGroup
}

//DiscoverReplications will query the replications api endpoint to attempt
//to discovery active replications on the Org
func (o *Org) DiscoverReplications() error {
	var repQueryURL string
	var mtx sync.Mutex
	repChan := make(chan int, 50)
	var wg sync.WaitGroup
	for _, link := range o.Links {
		if link.Action == "down:replications" {
			repQueryURL = link.URL
			break
		}
	}
	if repQueryURL == "" {
		return errors.New("Could not locate replications endpoint URL")
	}

	var repRefs struct {
		References []QueryReference `xml:"Reference"`
		Pagesize   int              `xml:"pageSize,attr"`
		Total      int              `xml:"total,attr"`
		Page       int
	}

	Debug("Fetching replication query pages")
	o.Replications = make(map[string]*ReplicationGroup, 10)
	repRefs.Total = 1
	for (repRefs.Page * repRefs.Pagesize) < repRefs.Total {
		repRefs.References = nil
		repRefs.Page++
		fullURL := fmt.Sprintf("%v%v%v", repQueryURL, "?page=", repRefs.Page)
		err := o.session.FetchAndUnmarshal(fullURL, &repRefs)
		if err != nil {
			return errors.Wrap(err, "Could not fetch replication query page(s)")
		}
		for _, ref := range repRefs.References {
			repChan <- 1
			wg.Add(1)
			go func(u string) {
				defer wg.Done()
				rep, _ := o.loadReplicationData(u)
				if rep != nil {
					mtx.Lock()
					o.Replications[rep.UUID] = rep
					mtx.Unlock()
				}
				<-repChan
			}(ref.URL)
		}
		wg.Wait()
	}
	return nil
}

func (o *Org) loadReplicationData(url string) (*ReplicationGroup, error) {
	rg := new(ReplicationGroup)

	Debug("Loading individual server replication data")
	err := o.session.FetchAndUnmarshal(url, rg)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to load rep details")
	}
	rg.session = o.session
	rg.UUID = rg.URL[len(rg.URL)-uuidl:] //Last 36 chars of URL are UUID
	return rg, nil
}

//DisasterFailover will initate a failover (or test failover) event against
//the org.
func (o *Org) DisasterFailover(vms []string, test, powerOn, ignoreErrors bool, maxConcurrent int) error {
	var errBuffer int
	var isErr bool
	if maxConcurrent > 1 {
		ignoreErrors = true
	}
	if ignoreErrors {
		errBuffer = len(vms)
	}
	queue := make(chan int, maxConcurrent)
	taskError := make(chan bool, errBuffer)
	var wg sync.WaitGroup
	Debug("Starting DisasterFailover")
	orgCopy := *o
	for _, uuid := range vms {
		rep, ok := orgCopy.Replications[uuid]
		if !ok {
			continue
		}
		queue <- 1
		wg.Add(1)
		go func(r *ReplicationGroup) {
			Info("Beginning test/failover for server: " + r.Name)
			defer func() { <-queue; wg.Done() }()
			task, err := r.Failover(powerOn, test)
			if err != nil {
				if _, ok := err.(*missingAction); ok {
					//Assume box is already failed over
					Debug("Could not find failover action. Assuming already failed.")
					taskError <- false
					return
				}
				Error(err.Error())
				taskError <- true
				return
			}
			task.VMName = r.Name
			CheckTask(task, taskError)
		}(rep)
		if !ignoreErrors {
			isErr = <-taskError
			if isErr {
				Error("Error encountered during failover. Aborting remaining failovers")
				break
			}
		}
	}
	wg.Wait()
	close(taskError)
	if isErr && !ignoreErrors {
		return errors.New("Error during failover")
	}
	return nil
}

//CreateUUIDNameMap generates a mapping between UUIDs and VM
//friendly names
func (o *Org) CreateUUIDNameMap() map[string]string {
	nm := make(map[string]string)
	for uuid, rg := range o.Replications {
		nm[uuid] = rg.Name
	}
	return nm
}

//QueryReference represents vCD reference type returned when using
//the query endpoint
type QueryReference struct {
	VCDType string `xml:"type,attr"`
	URL     string `xml:"href,attr"`
}

//ReplicationGroup is a vCA DR replication object
type ReplicationGroup struct {
	CommonProps
	Actions []Link `xml:"Link"`
	UUID    string
}

//Failover will activate the disaster failover of a particular ReplicationGroup
func (r *ReplicationGroup) Failover(powerOn, test bool) (*Task, error) {
	var fURL string
	var cType string
	var oType string
	var payload []byte
	var err error

	Debug("Executing failover action")
	Debug("Refreshing rep group endpoint...")
	r.Refresh()

	var tfp TestFailoverParams
	tfp.Power = powerOn
	tfp.NS = vrnamespace
	tfp.Name = r.UUID

	var fp FailoverParams
	fp.Power = powerOn
	fp.NS = vrnamespace
	fp.Name = r.UUID

	if test {
		cType = "application/vnd.vmware.hcs.testFailoverParams+xml"
		oType = "operation:testFailover"
		payload, err = xml.Marshal(&tfp)
	} else {
		cType = "application/vnd.vmware.hcs.FailoverParams+xml"
		oType = "operation:failover"
		payload, err = xml.Marshal(&fp)
	}

	if err != nil {
		return nil, errors.Wrap(err, "Failed to marshal failover XML")
	}

	for _, action := range r.Actions {
		if action.Action == oType {
			fURL = action.URL
			break
		}
	}
	if fURL == "" {
		return nil, &missingAction{}
	}

	req, _ := http.NewRequest("POST", fURL, bytes.NewReader(payload))
	req.Header.Set("Content-Type", cType)
	Debug("Sending failover HTTP API request")
	resp, err := r.session.DoAPIRequest(req)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to send DR failover call")
	}
	task := new(Task)
	if err = Unmarshal(resp, task); err != nil {
		return nil, errors.Wrap(err, "Failed to unmarshal task XML")
	}
	task.session = r.session
	return task, nil
}

//Refresh queries the vCD API URL for the object itself and re-marshals the
//XML data
func (r *ReplicationGroup) Refresh() {
	r.Actions = nil
	if err := r.session.FetchAndUnmarshal(r.URL, r); err != nil {
		Error("Error refreshing replication group")
	}
}

//TestFailover will initate a test failover of the replication group
func (r *ReplicationGroup) TestFailover(powerOn bool) (*Task, error) {
	return r.Failover(powerOn, true)
}

//TestCleanup will run a cleanup operations on a replication that has been
//put into test-failover state
func (r *ReplicationGroup) TestCleanup() error {
	var cURL string
	r.Refresh()
	for _, action := range r.Actions {
		if action.Action == "operation:testCleanup" {
			cURL = action.URL
			break
		}
	}
	if cURL == "" {
		return errors.New("Unable to locate Cleanup action")
	}
	_, err := r.session.Post(cURL, nil)
	return errors.Wrap(err, "Unable to execute cleanup action")
}

//FailoverParams represents the XML structure sent when initiating
//a DR failover
type FailoverParams struct {
	XMLName xml.Name `xml:"FailoverParams"`
	SharedParams
}

//TestFailoverParams represents the XML structure sent when initiating
//a test failover
type TestFailoverParams struct {
	XMLName     xml.Name `xml:"TestFailoverParams"`
	Synchronize bool
	SharedParams
}

//SharedParams are shared XML tags/attributes between
//TestFailoverParams and FailoverParams
type SharedParams struct {
	Power bool   `xml:"PowerOn"`
	Name  string `xml:"name,attr"`
	NS    string `xml:"xmlns,attr"`
}

//CheckTask will monitor a vCloud Director task until it completes or has been
//active for 10 minutes
func CheckTask(task *Task, done chan bool) {
	Debug("Received task")
	var taskError bool
	expire := time.Now().Add(time.Minute * 10)
	for time.Now().Before(expire) {
		s := fmt.Sprintf("%v Progress: %v%%", task.VMName, task.Progress)
		Info(s)
		fmt.Println(s)
		prgs := task.Progress
		if task.Progress == 100 {
			taskError = false
			if task.Error.Message != "" {
				Error(fmt.Sprintf("Task finished but operation for %v failed: %v",
					task.VMName, task.Error.Message))
				taskError = true
			}
			break
		}
		time.Sleep(15 * time.Second)
		task.Refresh()
		if prgs != task.Progress {
			expire = time.Now().Add(time.Minute * 10)
		}
	}
	if task.Progress != 100 {
		Error(fmt.Sprintf("Task timed out for %v", task.VMName))
		taskError = true
	}
	done <- taskError
}
