package main

//go:generate go-bindata -o assets.go static/...

import (
	"crypto/subtle"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"sync"
	"time"

	"github.com/gorilla/mux"
	"github.com/pkg/errors"
	"github.com/satori/go.uuid"
)

const DEBUGMODE = false

type Cache struct {
	sync.RWMutex
	Cloud        *CloudConfig
	Org          *Org `json:"-"`
	Orphans      []string
	Mappings     map[string]string
	cStart, cEnd chan int
	failActs     chan FailAction
}

type ServerState struct {
	Password  string
	Sessions  map[string]bool
	LastLogin time.Time
	LoginAtt  int
	sync.RWMutex
}

type FailAction struct {
	Servers       []string
	PowerOn       bool
	IgnoreErrors  bool
	MaxConcurrent int
	Test          bool
	Org           *Org `json:"-"`
}

var servCache map[string]*Cache
var servState ServerState
var gMtx sync.RWMutex

func httpRedirect(port string) {
	srv := &http.Server{
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
		Handler: http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
			w.Header().Set("Connection", "close")
			url := "https://" + req.Host + ":" + port + req.URL.String()
			http.Redirect(w, req, url, http.StatusMovedPermanently)
		}),
	}
	srv.ListenAndServe()
}

func setupTLS() *tls.Config {
	t := &tls.Config{
		PreferServerCipherSuites: true,
		CurvePreferences:         []tls.CurveID{tls.CurveP256},
		MinVersion:               tls.VersionTLS12,
	}
	return t
}

func newCache() *Cache {
	c := &Cache{
		Cloud:    new(CloudConfig),
		Org:      nil,
		cStart:   make(chan int, 1),
		cEnd:     make(chan int),
		failActs: make(chan FailAction, 25),
	}
	return c
}

//StartServer loads the config file data into an HTTP(S) Server
//and starts the server listening
func StartServer(c *Config) {
	var err error
	addr := c.IPAddr + ":" + c.Port
	servCache = make(map[string]*Cache)
	servState = ServerState{
		Password: c.Password,
		Sessions: make(map[string]bool),
	}
	r := mux.NewRouter()
	setupHandles(r)
	s := &http.Server{
		Addr:         addr,
		Handler:      r,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	if c.CertFile != "" && c.KeyFile != "" {
		s.TLSConfig = setupTLS()
	}

	for name, cloud := range c.Clouds {
		nc := newCache()
		nc.Cloud = cloud
		servCache[name] = nc
		go FailoverProcessor(nc.failActs)
	}
	fmt.Printf("Server starting on %s, port %s\n", c.IPAddr, c.Port)
	if s.TLSConfig != nil {
		go httpRedirect(c.Port)
		fmt.Println("Server running in HTTPS mode")
		err = s.ListenAndServeTLS(c.CertFile, c.KeyFile)
	} else {
		fmt.Println("Server running in HTTP mode")
		err = s.ListenAndServe()
	}
	if err != nil {
		Error(err.Error())
	}
}

func setupHandles(r *mux.Router) {
	Debug("Setting up handlers")
	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		Debug("Calling index redirect handler")
		http.Redirect(w, r, "/static/index.html", 302)
	})

	r.HandleFunc("/login", loginHandler)
	r.HandleFunc("/log", authMiddleware(logHandler))
	r.PathPrefix("/static/").HandlerFunc(authMiddleware(staticHandler))
	r.HandleFunc("/clouds/", authMiddleware(cloudListHandler)).Methods("GET")
	r.HandleFunc("/clouds/", authMiddleware(addCloudHandler)).Methods("POST")
	r.HandleFunc("/clouds/{name}", authMiddleware(cloudRefreshHandler)).Methods("GET")
	r.HandleFunc("/clouds/{name}", authMiddleware(updateCloudHandler)).Methods("PUT")
	r.HandleFunc("/clouds/{name}/Failover/", authMiddleware(cloudFailoverHandler)).Methods("POST")
	r.HandleFunc("/clouds/{name}/testFailover/", authMiddleware(cloudFailoverHandler)).Methods("POST")
}

func authMiddleware(fn http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if DEBUGMODE {
			fn(w, r)
			return
		}
		cookie, err := r.Cookie("session")
		rw := r.Header.Get("X-Requested-With")
		value := "dummyvalue"
		if err == nil {
			value = cookie.Value
		}
		servState.RLock()
		active, ok := servState.Sessions[value]
		servState.RUnlock()
		if err != nil || !ok || !active {
			if rw == "XMLHttpRequest" {
				http.Error(w, "Unauthorized -- Please press F5/Refresh this page and log in", 401)
			} else {
				http.Redirect(w, r, "/login", 303)
			}
			return
		}
		cookie.Expires = time.Now().Add(time.Hour)
		cookie.Path = "/"
		http.SetCookie(w, cookie)
		fn(w, r)
	}
}

func loginHandler(w http.ResponseWriter, r *http.Request) {
	page, err := Asset("static/login.html")
	if err != nil {
		Error(fmt.Sprintf("Error serving static asset: %s", err.Error()))
		http.NotFound(w, r)
		return
	}
	servState.RLock()
	var badLogins bool
	if time.Since(servState.LastLogin) < time.Minute*5 && servState.LoginAtt >= 2 {
		w.Write([]byte("Too many invalid logins"))
		badLogins = true
	}
	servState.RUnlock()
	if badLogins {
		return
	}
	if strings.ToLower(r.Method) == "get" {
		w.Write(page)
		return
	}
	servState.RLock()
	password := servState.Password
	servState.RUnlock()
	r.ParseForm()

	if subtle.ConstantTimeCompare([]byte(password),
		[]byte(r.FormValue("password"))) == 1 {
		//successful login
		Info("Successful login from "+r.RemoteAddr)
		var cookie *http.Cookie
		var err error
		cookie, err = r.Cookie("session")
		if err != nil {
			cookie = &http.Cookie{
				Name:  "session",
				Value: uuid.NewV4().String(),
				Path:  "/",
			}
		}
		cookie.Expires = time.Now().Add(time.Hour)
		servState.Lock()
		servState.Sessions[cookie.Value] = true
		servState.Unlock()
		http.SetCookie(w, cookie)
		http.Redirect(w, r, "/static/index.html", 303)
	} else {
		//Bad login
		Info("FAILED login from "+r.RemoteAddr)
		if time.Since(servState.LastLogin) < time.Minute*5 {
			servState.Lock()
			servState.LoginAtt++
			servState.Unlock()
		} else {
			servState.Lock()
			servState.LoginAtt = 1
			servState.Unlock()
		}
		servState.LastLogin = time.Now()
	}
	w.Write(page)
}

func logHandler(w http.ResponseWriter, r *http.Request) {
	w.Write(DumpBuffer())
}

func staticHandler(w http.ResponseWriter, r *http.Request) {
	Debug("Calling static asset handler with " + r.URL.Path[1:])
	buf, err := Asset(r.URL.Path[1:])
	if err != nil {
		Error(fmt.Sprintf("Error serving static asset: %s", err.Error()))
		http.NotFound(w, r)
		return
	}
	if strings.HasSuffix(r.URL.Path, ".css") {
		w.Header().Set("Content-Type", "text/css;charset=utf-8")
	} else if strings.HasSuffix(r.URL.Path, ".js") {
		w.Header().Set("Content-Type", "application/javascript;charset=utf-8")
	}
	w.Write(buf)
}

func cloudListHandler(w http.ResponseWriter, r *http.Request) {
	//Handles GET /clouds/
	Debug("Calling cloudListHandler")
	w.Header().Set("Content-Type", "application/json")
	gMtx.RLock()
	defer gMtx.RUnlock()
	jEnc := json.NewEncoder(w)
	if err := jEnc.Encode(servCache); err != nil {
		Error(err.Error())
		http.Error(w, "Internal Server Error", 500)
	}
}

func cloudRefreshHandler(w http.ResponseWriter, r *http.Request) {
	//Handles GET /clouds/{name}
	var err error
	name := mux.Vars(r)["name"]

	Debug("Calling cloudRefreshHandler")
	gMtx.RLock()
	cache, ok := servCache[name]
	gMtx.RUnlock()
	if !ok {
		Error(fmt.Sprintf("%v is not in cache", name))
		http.Error(w, "Cannot locate cloud", 500)
		return
	}

	select {
	case cache.cStart <- 1:
		go func() {
			//Do processing here
			Debug("Starting cloudRefreshHandler Populate goroutine")
			var code int
			cache.Lock()
			if cache.Org == nil {
				//Org does not exist in the cache
				s := NewSession(cache.Cloud.Username, cache.Cloud.Password,
					cache.Cloud.OrgName, cache.Cloud.URL, apiversion)
				cache.Org, err = s.PopulateCloudDetails()
				if err != nil {
					Error(err.Error())
					code = 500
				}
			} else if err = cache.Org.DiscoverReplications(); err != nil {
				Error(err.Error())
				code = 500
			}
			if code == 0 {
				cache.Mappings = cache.Org.CreateUUIDNameMap()
			}
			cache.Unlock()
			cache.cEnd <- code
			<-cache.cStart
		}()
		http.Error(w, "Not Ready", 503)
		return
	case code := <-cache.cEnd:
		//Do marshalling here
		Debug("cloudRefreshHandler end case processing")
		if code == 500 {
			http.Error(w, "Unable to gather details from provider", code)
			return
		}
		cache.Lock()
		checkOrphans(cache)
		body, err := json.Marshal(cache)
		if err != nil {
			Error(err.Error())
			http.Error(w, "Could not create JSON data", 500)
			cache.Unlock()
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(body)
		cache.Cloud.SaveConfig(name)
		cache.Unlock()
		return
	default:
		//Pass through, can't send/receive from either channel
		Debug("cloudRefreshHandler default case processing")
		http.Error(w, "Not Ready", 503)
	}
}

func addCloudHandler(w http.ResponseWriter, r *http.Request) {
	//Handles /clouds/ POST
	var nc struct {
		Name string
	}
	Debug("Adding new cloud")
	if err := UnmarshalJSON(r, &nc); err != nil {
		Error(err.Error())
		http.Error(w, "Failed to create cloud", 500)
		return
	}
	gMtx.Lock()
	defer gMtx.Unlock()
	if _, ok := servCache[nc.Name]; ok {
		http.Error(w, "Cloud with that name already exists", 409)
		return
	}
	nCache := newCache()
	servCache[nc.Name] = nCache
	go FailoverProcessor(nCache.failActs)
	servCache[nc.Name].Cloud.SaveConfig(nc.Name)
	w.Write([]byte(""))
}

func updateCloudHandler(w http.ResponseWriter, r *http.Request) {
	//Handles /clouds/{name} PUT
	Debug("Updating cloud")
	name := mux.Vars(r)["name"]
	gMtx.RLock()
	c, ok := servCache[name]
	gMtx.RUnlock()
	if !ok {
		http.Error(w, "No cloud with that name exists", 500)
		return
	}
	conf := new(CloudConfig)
	if err := UnmarshalJSON(r, conf); err != nil {
		Error(err.Error())
		http.Error(w, "Unable to save cloud config", 500)
		return
	}
	c.Lock()
	c.Cloud = conf
	c.Org = nil
	conf.SaveConfig(name)
	c.Unlock()
}

func cloudFailoverHandler(w http.ResponseWriter, r *http.Request) {
	name := mux.Vars(r)["name"]
	test := strings.HasSuffix(r.URL.Path, "testFailover/")
	Debug("Starting cloudFailoverHandler")
	gMtx.RLock()
	cache, ok := servCache[name]
	gMtx.RUnlock()
	if !ok {
		Error("Could not locate entry for cloud: " + name)
		http.Error(w, "Could not locate cloud entry", 500)
		return
	}
	cache.RLock()
	if cache.Org == nil {
		http.Error(w, "Cloud details not accurate. Please refresh cloud!", 500)
		cache.RUnlock()
		return
	}
	cache.RUnlock()
	var pl FailAction
	err := UnmarshalJSON(r, &pl)
	if err != nil {
		Error("JSON Error: " + err.Error())
		http.Error(w, "Unable to parse request data", 500)
		return
	}
	pl.Test = test
	cache.Lock()
	defer cache.Unlock()
	if pl.MaxConcurrent > 10 {
		pl.MaxConcurrent = 10
	} else if pl.MaxConcurrent < 1 {
		pl.MaxConcurrent = 1
	}
	pl.Org = cache.Org

	Info("Failover action initated by "+r.RemoteAddr)
	select {
	case cache.failActs <- pl:
		return
	default:
		http.Error(w, "Cannot add failover request to queue. Queue is full (max 25)", 500)
	}
}

func flattenGroups(grps []RepGroup) map[string]bool {
	res := make(map[string]bool)
	for _, g := range grps {
		for _, vm := range g {
			res[vm] = true
		}
	}
	return res
}

func checkOrphans(c *Cache) {
	Debug("Checking for orphans")
	allVMs := flattenGroups(c.Cloud.Groups)
	var orphans []string
	for uuid := range c.Org.Replications {
		if _, ok := allVMs[uuid]; !ok {
			//Cloud UUID does not exist in groups
			orphans = append(orphans, uuid)
		}
	}
	Debug("Checking for removed cloud entries")
	var nGroups []RepGroup
	for _, grp := range c.Cloud.Groups {
		var nGrp RepGroup
		for _, uuid := range grp {
			if _, ok := c.Org.Replications[uuid]; ok {
				nGrp = append(nGrp, uuid)
			}
		}
		if nGrp != nil {
			nGroups = append(nGroups, nGrp)
		}
	}
	c.Cloud.Groups = nGroups
	c.Orphans = orphans
}

//UnmarshalJSON will take the JSON from a request body and unmarshall it into
//the provided interface
func UnmarshalJSON(r *http.Request, v interface{}) error {
	b, _ := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err := json.Unmarshal(b, v); err != nil {
		return errors.Wrap(err, "Failed to Unmarshal JSON")
	}
	return nil
}

//FailoverProcessor runs in goroutines to check for failover actions added to
//the queue channel. It runs each action in sequence and blocks until each action
//is complete
func FailoverProcessor(c chan FailAction) {
	for fo := range c {
		if fo.Org == nil {
			continue
		}
		err := fo.Org.DisasterFailover(fo.Servers, fo.Test, fo.PowerOn, fo.IgnoreErrors, fo.MaxConcurrent)
		if err != nil {
		Drain:
			for {
				select {
				case <-c:
				default:
					break Drain
				}
			}
		}
	}
}
