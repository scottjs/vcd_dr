package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os"
)

var key = []byte("aqs23f95a2ei6rztp6b6h67ay2quy9a4")

//RepGroup represents a replication group of one or more
//servers
type RepGroup []string

//Config represents the global config object which is read from the
//config.json file. It may contain multiple DR cloud configs
type Config struct {
	IPAddr   string
	Port     string
	CertFile string
	KeyFile  string
	Password string
	Clouds   map[string]*CloudConfig
}

//CloudConfig is the login config for a single vCD cloud.
//It also contains a slice of replication groups
type CloudConfig struct {
	Username string
	Password string
	URL      string
	OrgName  string
	Groups   []RepGroup
}

//NewConfig creates a default config object
func NewConfig() *Config {
	c := &Config{
		IPAddr:   "127.0.0.1",
		Port:     "8080",
		Password: "admin",
		Clouds:   make(map[string]*CloudConfig),
	}
	cc := new(CloudConfig)
	cc.Groups = make([]RepGroup, 1)
	cc.Groups[0] = RepGroup{"UUID1", "UUID2"}
	c.Clouds["DRCloud1"] = cc
	c.SaveConfig("")
	return c
}

//LoadConfig loads the config.json file from disk
func LoadConfig() (*Config, error) {
	f, err := os.Open("config.json")
	if err != nil {
		Info("Could not open 'config.json', writing a new config file.")
		return NewConfig(), nil
	}
	cbts, _ := ioutil.ReadAll(f)
	f.Close()
	c := new(Config)
	err = json.Unmarshal(cbts, c)
	if err != nil {
		return nil, err
	}
	for _, cloud := range c.Clouds {
		cloud.Password = decryptPassword(cloud.Password)
	}
	return c, nil
}

//SaveConfig saves the specified config file to disk
func (c Config) SaveConfig(file string) {
	if file == "" {
		file = "config.json"
	}
	f, err := os.Create(file)
	if err != nil {
		fmt.Println("Cannot create config file")
		return
	}
	defer f.Close()

	copyClouds := make(map[string]*CloudConfig)
	for name, cloud := range c.Clouds {
		cldCopy := *cloud
		cldCopy.Password = encryptPassword(cloud.Password)
		copyClouds[name] = &cldCopy
	}
	c.Clouds = copyClouds
	je := json.NewEncoder(f)
	je.SetIndent("", "    ")
	err = je.Encode(c)
	if err != nil {
		fmt.Println("Cannot convert config to JSON")
	}
}

//SaveConfig saves a cloud config to the main config file
func (c *CloudConfig) SaveConfig(n string) {
	Debug("Saving cloud config")
	conf, err := LoadConfig()
	if err != nil {
		return
	}
	conf.Clouds[n] = c
	conf.SaveConfig("")
}

// encrypt string to base64 crypto using AES
func encryptPassword(text string) string {
	plaintext := []byte(text)

	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}

	ciphertext := make([]byte, aes.BlockSize+len(plaintext))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		panic(err)
	}

	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(ciphertext[aes.BlockSize:], plaintext)

	// convert to base64
	return base64.URLEncoding.EncodeToString(ciphertext)
}

// decrypt from base64 to decrypted string
func decryptPassword(cryptoText string) string {
	ciphertext, err := base64.URLEncoding.DecodeString(cryptoText)
	if err != nil {
		return cryptoText
	}

	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}

	if len(ciphertext) < aes.BlockSize {
		panic("ciphertext too short")
	}
	iv := ciphertext[:aes.BlockSize]
	ciphertext = ciphertext[aes.BlockSize:]

	stream := cipher.NewCFBDecrypter(block, iv)

	// XORKeyStream can work in-place if the two arguments are the same.
	stream.XORKeyStream(ciphertext, ciphertext)

	return fmt.Sprintf("%s", ciphertext)
}
