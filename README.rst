README
======

Cerberus
---------------------------------------------------------

Cerberus functions as both a command-line utility and a web-based 
utility for managing vCloud Air Disaster Recovery environments. The feature set is 
minimal and focuses on creating replication groups to fail over in sequential order. 
Virtual Machine UUIDs can be placed into groups and, using either the command-line or the 
web interface, failed (or tested) in the vCloud Air environment.

At this time test cleanup is unavailable as is any advanced monitoring of the failover process
once it has been initiated. If any VM encounters an error during the failover process, and ignore
errors is disabled, all subsequent VMs will not be failed over. This will leave the environment
in a partially failed state until corrected via human intervention. This is an intentional
design decision based on not being able to know whether a subsequent VM or group of VMs is dependent
upon a previous VM.

Currently, there is no mechanism in place to create any sort of dependency mapping or hierarchy between
VMs.
